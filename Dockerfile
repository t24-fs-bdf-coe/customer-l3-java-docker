FROM maven:3.8.4-jdk-11
COPY settings.xml /usr/share/maven/ref/
WORKDIR /app
ADD . /app
RUN mvn clean package -s settings.xml
RUN mvn sonar:sonar -s settings.xml -Dsonar.host.url=http://sonarlb-32595587.us-east-1.elb.amazonaws.com -Dsonar.login=b31660e78cd93261583d1593ef5aea46ea72759f


FROM gcr.io/temenos-dev/r20-appimage:t24app_latest
# Copy jar file to target folder
COPY --from=0 /app/target/*.jar /srv/Temenos/T24/Lib/t24lib
USER root
RUN chmod -R 0755 /srv/Temenos/T24/Lib/t24lib

# Copy and execute bash script
ADD file_entry_check.sh /srv/Temenos/deploy_scripts/file_entry_check.sh
RUN chmod -R 0755 /srv/Temenos/deploy_scripts
RUN /srv/Temenos/deploy_scripts/file_entry_check.sh customer-l3-java > /srv/Temenos/deploy_scripts/file_entry_check_output.txt
RUN cat /opt/jboss/wildfly/modules/com/temenos/t24/main/module.xml | grep customer-l3-java