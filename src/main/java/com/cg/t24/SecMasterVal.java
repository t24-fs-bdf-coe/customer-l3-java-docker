package main.java.com.cg.t24;

import com.temenos.api.TField;
import com.temenos.api.TStructure;
import com.temenos.api.TValidationResponse;
import com.temenos.t24.api.complex.eb.templatehook.TransactionContext;
import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.t24.api.system.Session;
import com.temenos.t24.api.records.secopenorder.SecOpenOrderRecord;
import com.temenos.t24.api.records.securitymaster.SecurityMasterRecord;
import com.temenos.t24.api.system.DataAccess;

/**
 * TODO: Document me!
 *
 * @author pkumar
 *
 */
public class SecMasterVal extends RecordLifecycle {

    private static String ERRORMESSAGE = "This record is invalid for this company"; 
    /**
     *
     */
    public TValidationResponse validateRecord(String application, String currentRecordId, TStructure currentRecord,
            TStructure unauthorisedRecord, TStructure liveRecord, TransactionContext transactionContext) {
        SecOpenOrderRecord secRec = new SecOpenOrderRecord(currentRecord);
        TField secNo = secRec.getSecurityNo();
        String secNoStr = secNo.getValue().toString();
        DataAccess da = new DataAccess(this);
        SecurityMasterRecord smRec = new SecurityMasterRecord(da.getRecord("SECURITY.MASTER", secNoStr));
        String compCode = smRec.getCoCode();
        Session ssObj = new Session(this);
        String sessionComp = ssObj.getCompanyId();
        
            if(!(sessionComp.equals(compCode)))
            {
                secNo.setError(ERRORMESSAGE);
            }
            return secRec.getValidationResponse();
    }
}
