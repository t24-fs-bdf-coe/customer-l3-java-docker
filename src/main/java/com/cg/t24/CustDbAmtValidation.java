package com.cg.t24;

import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.api.TField;
import com.temenos.api.TStructure;
import com.temenos.api.TValidationResponse;
import com.temenos.t24.api.records.fundstransfer.FundsTransferRecord;

/**
 * TODO: Document me!
 *
 * @author Subbu
 *
 */
public class CustDbAmtValidation extends RecordLifecycle {
    public TValidationResponse validateRecord(String application, String currentRecordId, TStructure currentRecord, TStructure unauthorisedRecord, TStructure liveRecord, com.temenos.t24.api.complex.eb.templatehook.TransactionContext transactionContext)
    {
        // TODO Auto-generated method stub
        
        String dbAmntError = "Transaction of amount more than 10,000 not permitted";
       
        FundsTransferRecord ftRecord = new FundsTransferRecord(currentRecord);   
        
        TField debitAmount = ftRecord.getDebitAmount();        
        String dbAmnt = debitAmount.getValue().toString();
        double amount = Double.parseDouble(dbAmnt);
        
         if (amount > 10000){
             debitAmount.setError(dbAmntError);
         }
          
         return ftRecord.getValidationResponse();
         }
}
