package main.java.com.cg.t24;

import com.temenos.api.TField;
import com.temenos.api.TStructure;
import com.temenos.api.TValidationResponse;
import com.temenos.t24.api.complex.eb.templatehook.TransactionContext;
import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.t24.api.records.fundstransfer.FundsTransferRecord;

/**
 * TODO: Document me!
 *
 * @author pkumar
 *
 */
public class InputRtn extends RecordLifecycle {

    @Override
    public TValidationResponse validateRecord(String application, String currentRecordId, TStructure currentRecord,
            TStructure unauthorisedRecord, TStructure liveRecord, TransactionContext transactionContext) {
        
        String dbAmntError = "Transaction of amount more than 10,000 is not permitted";

        FundsTransferRecord ftRecord = new FundsTransferRecord(currentRecord);  
        TField debitAmount = ftRecord.getDebitAmount();        
        String dbAmnt = debitAmount.getValue().toString();
        double amount = Double.parseDouble(dbAmnt);
        if (amount > 10000){
            debitAmount.setError(dbAmntError);
        }
        return ftRecord.getValidationResponse();
    }
}
