#!/usr/bin/env groovy
/**
 * The Project "customer-l3-java" offers CI/CD pipelines to automate build and deployment processes for customer-l3-java. The corresponding *"BuildJenkinsfile.groovy" invokes "bpi-t24-shared-libraries" to automate build process for T24 CoreBanking customer-l3-java module. 
 * Author: Subbu
 * Email : subramani.a.murugesan@capgemini.com
 */

@Library('bpi-t24-shared-libraries')

def buildParam = [
    "applicationName" : "customer-l3-java",
    "configPath": "JenkinsFiles/build/build.yaml",
	"node": null,
    "properties": [
        "maxBuildsToKeep": "10" // we'd like to make sure we only keep 10 builds at a time, so we don't fill up our storage!
    ]
]

t24javabuildPipeline(buildParam)